import { Component } from '@angular/core';
import { Movie } from './shared/movie.modal';
import { MovieService } from './shared/movie.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  movie!: Movie[];

  loading: boolean = false;

  constructor(private moveService: MovieService){}
}
