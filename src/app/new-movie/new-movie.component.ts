import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MovieService } from '../shared/movie.service';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { Movie } from '../shared/movie.modal';

@Component({
  selector: 'app-new-movie',
  templateUrl: './new-movie.component.html',
  styleUrls: ['./new-movie.component.css']
})
export class NewMovieComponent implements OnInit, OnDestroy {
  @ViewChild('titleInput') titleInput!: ElementRef;
  @ViewChild('addButton') addButton!: ElementRef;

  isUploading = false;
  movieUploadingSubscription!: Subscription;

  constructor(
    public movieService: MovieService,
    public http: HttpClient
  ) {}

  ngOnInit(): void {
    this.movieUploadingSubscription = this.movieService.movieUploading.subscribe((isUploading: boolean) =>{
      this.isUploading = isUploading;
    });
  }

  createMovie(){
    const title = this.titleInput.nativeElement.value;
    const id = Math.random().toString();
    const movie = new Movie(id, title);

    this.movieService.addMovie(movie).subscribe(() =>{
      this.movieService.fetchMovies();
    })
  }

  ngOnDestroy(): void {
    this.movieUploadingSubscription.unsubscribe();
  }
}
