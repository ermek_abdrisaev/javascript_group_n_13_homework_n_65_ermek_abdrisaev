import { HttpClient } from '@angular/common/http';
import { Subject, Subscription } from 'rxjs';
import { Movie } from './movie.modal';
import { map, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable()

export class MovieService {
  moviesChange = new Subject<Movie[]>();
  moviesFetching = new Subject<boolean>();
  movieUploading = new Subject<boolean>();

  constructor(private http: HttpClient) {
  }

  private movies: Movie[] = [];

  fetchMovies() {
    this.moviesFetching.next(true);

    this.http.get<{ [id: string]: Movie }>('https://jsclasswork-e9b16-default-rtdb.firebaseio.com/movies.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const movieData = result[id];
          return new Movie(id, movieData.title);
        });
      }))
      .subscribe(movies => {
        this.movies = movies;
        this.moviesChange.next(this.movies.slice());
        this.moviesFetching.next(false);
      }, () => {
        this.moviesFetching.next(false);
      });
  }

  getMovies() {
    return this.movies.slice();
  }

  addMovie(movie: Movie) {
    const body = {
      title: movie.title,
    };

    this.movieUploading.next(true);

    return this.http.post('https://jsclasswork-e9b16-default-rtdb.firebaseio.com/movies.json', body)
      .pipe(tap(() => {
          this.movieUploading.next(false);
        }, () => {
          this.movieUploading.next(false);
        })
      );
  };
}
