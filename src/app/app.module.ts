import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { NewMovieComponent } from './new-movie/new-movie.component';
import { MoviesToWatchComponent } from './movies-to-watch/movies-to-watch.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MovieItemComponent } from './movies-to-watch/movie-item/movie-item.component';
import { MovieService } from './shared/movie.service';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    NewMovieComponent,
    MoviesToWatchComponent,
    MovieItemComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [MovieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
