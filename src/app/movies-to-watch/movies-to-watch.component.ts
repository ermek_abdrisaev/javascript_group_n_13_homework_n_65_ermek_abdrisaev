import { Component, OnDestroy, OnInit } from '@angular/core';
import { Movie } from '../shared/movie.modal';
import { Subscription } from 'rxjs';
import { MovieService } from '../shared/movie.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-movies-to-watch',
  templateUrl: './movies-to-watch.component.html',
  styleUrls: ['./movies-to-watch.component.css']
})
export class MoviesToWatchComponent implements OnInit, OnDestroy {
  movies!: Movie[];
  moviesChangeSubscription!: Subscription;
  moviesFetchingSubscription!: Subscription;
  loading: boolean = false;


  constructor(
    private movieService: MovieService,
    private http: HttpClient
  ) {}

  ngOnInit(): void {
    this.movies = this.movieService.getMovies();
    this.moviesChangeSubscription = this.movieService.moviesChange.subscribe((movies: Movie[]) =>{
      this.movies = movies;
    });
    this.moviesFetchingSubscription = this.movieService.moviesFetching.subscribe((isFetching: boolean) =>{
      this.loading = isFetching;
    });
    this.movieService.fetchMovies();
  }

  ngOnDestroy(){
    this.moviesChangeSubscription.unsubscribe();
    this.moviesFetchingSubscription.unsubscribe();
  }
}
