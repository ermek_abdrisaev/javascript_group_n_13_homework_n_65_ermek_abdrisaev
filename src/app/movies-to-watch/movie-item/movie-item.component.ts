import { Component, Input, OnInit } from '@angular/core';
import { Movie } from '../../shared/movie.modal';
import { MovieService } from '../../shared/movie.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-movie-item',
  templateUrl: './movie-item.component.html',
  styleUrls: ['./movie-item.component.css']
})
export class MovieItemComponent implements OnInit {
  @Input() movie!: Movie;
  movieId = '';
  loading = false;


  constructor(
    private movieService: MovieService,
    private http: HttpClient,
    private  route: ActivatedRoute) {
  }

  ngOnInit(): void {
      this.movieId = this.movie.id;
  }

  deleteMovie(id: string){
      console.log(this.movie);
      this.http.delete(`https://jsclasswork-e9b16-default-rtdb.firebaseio.com/movies/${id}.json`).subscribe(()=>{
        this.movieService.fetchMovies();
      });
  }
}
